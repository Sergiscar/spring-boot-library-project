package com.library.sergi.entity.operation;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.library.sergi.entity.book.Livre;
import com.library.sergi.entity.user.Role;
import com.library.sergi.entity.user.User;
import jakarta.persistence.*;
import lombok.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "operation")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//@JsonSerialize(using = OperationSerializer.class)
//@JsonIgnoreProperties({"livre","user"})
public class Operation {
    @Id
    @GeneratedValue
    private Integer id;
    private Integer quantite;
    @Enumerated(EnumType.STRING)
    private State state;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Timestamp timestamp;
    private Integer montant;
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "livre_id")
    private Livre livre;
    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @PrePersist
    protected void onCreate() {
        timestamp = Timestamp.valueOf(LocalDateTime.now());
    }
}
