package com.library.sergi.entity.operation;

public enum State {
    PANIER,
    INVALIDATED,
    VALIDATED
}
