package com.library.sergi.entity.operation;

import com.library.sergi.entity.book.Livre;
import com.library.sergi.entity.category.Categorie;
import com.library.sergi.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.webmvc.RepositoryRestController;

import java.util.List;

@RepositoryRestController(path="operations")
public interface OperationRepository extends JpaRepository<Operation, Integer> {
    List<Operation> findByLivreId(Integer livreId);
    List<Operation> findByUserId(Integer userId );
   // @Query(value = "SELECT * FROM operation WHERE user_id = :userId AND state = :panier", nativeQuery = true)
    //List<Operation> findPanierByUser(@Param("userId") Integer userId, @Param("panier") String panier);
    List<Operation> findByUserAndState(User user, State state);
   /* @Query(value= "UPDATE operation e SET e.state = :newValue WHERE e.user_id = :id AND e.state = :state", nativeQuery = true)
    int validatePanier(@Param("id") Integer id, @Param("state") State state, @Param("newValue") State newValue);
    @Query(value= "UPDATE operation e SET e.state = :newValue WHERE e.id = :id ", nativeQuery = true)
    int deleteFromPanier(@Param("id") Integer id, @Param("newValue") State newValue);
    @Query(value = "SELECT * FROM operation WHERE user_id = :userId AND panier = :panier" , nativeQuery = true)
    List<Operation> findByedByUser(@Param("userId") Integer userId,@Param("panier") State panier);*/
    @Query(value = "SELECT op.livre_id, SUM(op.quantite) AS totalBuyCount " +
            "FROM Operation op " +
            "GROUP BY op.livre_id " +
            "ORDER BY totalBuyCount DESC",nativeQuery = true)
    List<Object[]> findMostBoughtBooks();
   /*  @Query(value = "SELECT op.montant, SUM(op.) AS totalBuyCount " +
            "FROM Operation op " +
            "GROUP BY op.montant " +
            "ORDER BY totalBuyCount DESC",nativeQuery = true)
    List<Operation> findMostBusinessBooks();*/

}
