package com.library.sergi.entity.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class OperationSerializer extends JsonSerializer<Operation> {

    @Override
    public void serialize(Operation operation, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", operation.getId());
        jsonGenerator.writeNumberField("quantite", operation.getQuantite());
        jsonGenerator.writeStringField("state", operation.getState().toString());
        // Serialize other fields as needed

        // Handle circular references here if necessary
        // For example, serialize only the id of the associated Livre and User
        jsonGenerator.writeObjectFieldStart("livre");
        jsonGenerator.writeNumberField("id", operation.getLivre().getId());
        jsonGenerator.writeEndObject();

        jsonGenerator.writeObjectFieldStart("user");
        jsonGenerator.writeNumberField("id", operation.getUser().getId());
        jsonGenerator.writeEndObject();

        jsonGenerator.writeEndObject();
    }
}