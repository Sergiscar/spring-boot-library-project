package com.library.sergi.entity.book;

import com.library.sergi.entity.category.Categorie;
import com.library.sergi.entity.category.CategorieDto;
import com.library.sergi.entity.user.UserDto;
import lombok.Data;

@Data
public class LivreDto {
    private Integer id;
    private String titre;
    private String auteur;
    private Integer prix;
    private Integer quantite;
    private CategorieDto categorieDto;
    private UserDto userDto;
}
