package com.library.sergi.entity.book;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class LivreSerializer extends JsonSerializer<Livre> {

    @Override
    public void serialize(Livre livre, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", livre.getId());
        jsonGenerator.writeStringField("titre", livre.getTitre());
        jsonGenerator.writeStringField("auteur", livre.getAuteur());
        // Serialize other fields as needed

        // Handle circular references here if necessary
        // For example, serialize only the id of the associated Categorie and User
        jsonGenerator.writeObjectField("categorie", livre.getCategorie().getId());
        jsonGenerator.writeObjectField("user", livre.getUser().getId());

        jsonGenerator.writeEndObject();
    }
}
