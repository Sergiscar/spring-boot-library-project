package com.library.sergi.entity.book;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.library.sergi.entity.category.Categorie;
import com.library.sergi.entity.operation.Operation;
import com.library.sergi.entity.user.User;
import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "livre")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//@JsonSerialize(using = LivreSerializer.class)
//@JsonIgnoreProperties({"categorie", "user", "operations"})
public class Livre {
    @Id
    @GeneratedValue
    private Integer id;
    private String titre;
    private String auteur;
    private Integer prix;
    private Integer quantite;

    @ManyToOne
    @JoinColumn(name = "categorie_id") // Assuming this is the foreign key column in EntityB referencing EntityA
    @JsonIgnore
    private Categorie categorie;
    @ManyToOne
    @JoinColumn(name = "user_id") // Assuming this is the foreign key column in EntityB referencing EntityA
    @JsonIgnore
    private User user;
    @OneToMany(mappedBy = "livre",fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Operation> operations=new HashSet<>();



}
