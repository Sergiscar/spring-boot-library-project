package com.library.sergi.entity.book;

import com.library.sergi.entity.category.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

import java.util.List;

@RepositoryRestController(path="livres")
public interface LivreRepository  extends JpaRepository<Livre, Integer> {
    List<Livre> findByCategorieId(Integer categorieId );
    List<Livre> findByUserId(Integer userId);
}
