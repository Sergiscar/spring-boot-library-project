package com.library.sergi.entity.user;

public enum Role {
    USER,
    ADMIN
}
