package com.library.sergi.entity.user;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.library.sergi.entity.book.Livre;
import com.library.sergi.entity.operation.Operation;

import java.io.IOException;

public class UserSerializer extends JsonSerializer<User> {

    @Override
    public void serialize(User user, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", user.getId());
        jsonGenerator.writeStringField("firstname", user.getFirstname());
        jsonGenerator.writeStringField("lastname", user.getLastname());
        jsonGenerator.writeStringField("email", user.getEmail());
        // Serialize other fields as needed

        // Serialize role as a string
        jsonGenerator.writeStringField("role", user.getRole().toString());

        // Handle circular references here if necessary
        // For example, serialize only the ids of associated Operations and Livres
        jsonGenerator.writeArrayFieldStart("operations");
        for (Operation operation : user.getOperations()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", operation.getId());
            // Serialize other relevant fields of Operation
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();

        jsonGenerator.writeArrayFieldStart("livres");
        for (Livre livre : user.getLivres()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", livre.getId());
            // Serialize other relevant fields of Livre
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();

        jsonGenerator.writeEndObject();
    }
}
