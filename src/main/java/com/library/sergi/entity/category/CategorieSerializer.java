package com.library.sergi.entity.category;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.library.sergi.entity.book.Livre;

import java.io.IOException;

public class CategorieSerializer extends JsonSerializer<Categorie> {

    @Override
    public void serialize(Categorie categorie, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", categorie.getId());
        jsonGenerator.writeStringField("nom", categorie.getNom());
        // Serialize other fields as needed

        // Handle circular references here if necessary
        // For example, serialize only the id of the associated Livres
        jsonGenerator.writeArrayFieldStart("livres");
        for (Livre livre : categorie.getLivres()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", livre.getId());
            jsonGenerator.writeStringField("titre", livre.getTitre());
            // Serialize other relevant fields of Livre
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();

        jsonGenerator.writeEndObject();
    }
}
