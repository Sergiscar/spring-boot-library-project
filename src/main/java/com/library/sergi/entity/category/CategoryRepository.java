package com.library.sergi.entity.category;

import com.library.sergi.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

@RepositoryRestController(path="categories")
public interface CategoryRepository extends JpaRepository<Categorie, Integer> {

}
