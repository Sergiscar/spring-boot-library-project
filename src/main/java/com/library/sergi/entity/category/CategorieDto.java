package com.library.sergi.entity.category;

import lombok.Data;

@Data
public class CategorieDto {
    private Integer id;
    private String nom;
}
