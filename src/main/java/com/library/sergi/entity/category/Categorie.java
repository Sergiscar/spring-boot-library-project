package com.library.sergi.entity.category;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.library.sergi.entity.book.Livre;
import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "categorie")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//@JsonIgnoreProperties({"livres"})
//@JsonSerialize(using = CategorieSerializer.class)
public class Categorie {
    @Id
    @GeneratedValue
    private Integer id;
    private String nom;
    @OneToMany(mappedBy = "categorie", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Livre> livres=new HashSet<>();


}
