package com.library.sergi.demo;

import com.library.sergi.entity.book.Livre;
import com.library.sergi.entity.book.LivreDto;
import com.library.sergi.entity.book.LivreRepository;
import com.library.sergi.entity.category.Categorie;
import com.library.sergi.entity.category.CategorieDto;
import com.library.sergi.entity.category.CategoryRepository;
import com.library.sergi.entity.operation.Operation;
import com.library.sergi.entity.operation.OperationRepository;
import com.library.sergi.entity.operation.State;
import com.library.sergi.entity.user.User;
import com.library.sergi.entity.user.UserDto;
import com.library.sergi.entity.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class DemoController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OperationRepository operationRepository;
    @Autowired
    private CategoryRepository categorieRepository;
    @Autowired
    private LivreRepository livreRepository;
    @GetMapping
    public ResponseEntity<String> sayHello()
    {
        return ResponseEntity.ok("Hello from the secured endpoint");
    }
    //ajout d'un livre au panier
    @PostMapping("/panier/livre/{livreId}/user/{userId}")
    public Operation addReservation(@PathVariable Integer livreId, @PathVariable Integer userId, @RequestBody Operation operationData) {
        // Find the author by
        Livre livre = livreRepository.findById(livreId)
                .orElseThrow(() -> new RuntimeException("Livre not found with id: " + livreId));
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException("Livre not found with id: " + userId));
        Operation operation = new Operation();
        operation.setUser(user);
        operation.setLivre(livre);
        operation.setMontant(operationData.getQuantite()*livre.getPrix());
        operation.setQuantite(operationData.getQuantite());
        operation.setState(State.PANIER);
        livre.setQuantite(livre.getQuantite()-operationData.getQuantite());
        Operation operation1 = operationRepository.save(operation);
        livreRepository.save(livre);
        return operation1;
    }
    //ajout d'un livre
    @PostMapping("/livres/categorie/{categorieId}/user/{userId}")
    public LivreDto addBookToAuthor(@PathVariable Integer categorieId,@PathVariable Integer userId,@RequestBody Livre livre) {
        Categorie categorie = categorieRepository.findById(categorieId)
                .orElseThrow(() -> new RuntimeException("Categorie not found with id: " + categorieId));
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException("Categorie not found with id: " + userId));
        System.out.println(user);
        livre.setCategorie(categorie);
        livre.setUser(user);
        Livre savedLivre = livreRepository.save(livre);
        return mappedtoLivreDto(savedLivre);
    }
  //liste des livres par categorie
    @GetMapping("/categorie/{categorieId}/livres")
    public List<LivreDto> getLivreByCategorie(@PathVariable Integer categorieId) {
        List<Livre>livre = new ArrayList<Livre>();
        try {
            // Code that may cause a StackOverflowError
             livre = livreRepository.findByCategorieId(categorieId);
        } catch (StackOverflowError e) {
            System.out.println(e.getMessage());

            // Optionally, handle or rethrow the exception as needed
        }

        return livre.stream().map(livre1 -> mappedtoLivreDto(livre1)).collect(Collectors.toList());
    }
    //Afficher le panier de l'utilisateur
    @GetMapping("/panier/{userId}")
    public List<Operation> afficherPanier(@PathVariable Integer userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException("Categorie not found with id: " + userId));
        List<Operation> operations = operationRepository.findByUserAndState(user , State.PANIER);
        System.out.println(user);
        return operations;
    }
    @GetMapping("/transaction/{userId}")
    public List<Operation> affichertTransaction(@PathVariable Integer userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException("Categorie not found with id: " + userId));
        List<Operation> operations = operationRepository.findByUserAndState(user , State.VALIDATED);
        System.out.println(user);
        return operations;
    }
    //retirer un produit du panier
    @PostMapping("/panier/livre/{operationId}")
    public Operation retirerproduitPanier(@PathVariable Integer operationId) {
      //  Livre livre = livreRepository.findById(livreId).orElseThrow(() -> new RuntimeException("Categorie not found with id: " + livreId));
        Operation operation = operationRepository.findById(operationId).orElseThrow(() -> new RuntimeException("Operation not found with id: " + operationId));
        operation.setState(State.INVALIDATED);
        operationRepository.save(operation);
        return operation;
    }
    @PostMapping("/panier/user/{userId}/validate")
    public List<Operation> validerPanier(@PathVariable Integer userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException("Categorie not found with id: " + userId));
        List<Operation> operation = operationRepository.findByUserAndState(user, State.PANIER);
        for (Operation operations : operation) {
            operations.setState(State.VALIDATED);
        }
      operationRepository.saveAll(operation);
        return operation;
    }
    //Afficher les livres de l'utilisateur
    @GetMapping("/livres/plusAchete")
    public List<Map<String, Object>> afficherLivresPlusachete() {
        List<Object[]> results = operationRepository.findMostBoughtBooks();

        // Convert the list of Object arrays to a list of maps
        List<Map<String, Object>> books = results.stream()
                .map(arr -> {
                    Map<String, Object> bookMap = Map.of(
                            "livreId", arr[0],
                            "totalSold", arr[1]
                    );
                    return bookMap;
                })
                .collect(Collectors.toList());

        return books;
    }

    //modification des informations d'un livre
    @PutMapping("/{categorieId}/livres")
    public LivreDto updateCategorie(@PathVariable Integer livreId,@PathVariable Integer categorieId , @RequestBody Livre livre1) {
        Categorie categorie = categorieRepository.findById(categorieId).orElseThrow(() -> new RuntimeException("Categorie not found with id: " + categorieId));
        Livre livre = livreRepository.findById(livreId).orElseThrow(() -> new RuntimeException("Categorie not found with id: " + categorieId));
        if(!livre.getCategorie().getId().equals(categorie.getId())){
            System.out.println("livre sans categorie");
        }
        livre.setTitre(livre1.getTitre());
        livre.setPrix(livre1.getPrix());
        livre.setQuantite(livre1.getQuantite());
        livreRepository.save(livre);
        return mappedtoLivreDto(livre);
    }
    //suppression d'un livre
    @DeleteMapping("/{categorieId}/livres")
    public void deleteCategorie(@PathVariable Integer livreId,@PathVariable Integer categorieId ) {
        Livre livre = livreRepository.findById(livreId).orElseThrow(() -> new RuntimeException("Categorie not found with id: " + categorieId));
        livreRepository.delete(livre);
    }
    //liste des livres par utilisateur
    @GetMapping("user/{userId}/livres")
    public List<LivreDto> getLivreByUser(@PathVariable Integer userId) {
        List<Livre> livre = livreRepository.findByUserId(userId);
        System.out.println(livre);
        return livre.stream().map(livre1 -> mappedtoLivreDto(livre1)).collect(Collectors.toList());
    }



    private LivreDto mappedtoLivreDto(Livre livre){
        LivreDto livreDto =new LivreDto();
        CategorieDto categorieDto =new CategorieDto();
        UserDto userDto =new UserDto();
        livreDto.setId(livre.getId());
        livreDto.setTitre(livre.getTitre());
        livreDto.setPrix(livre.getPrix());
        livreDto.setAuteur(livre.getAuteur());
        livreDto.setQuantite(livre.getQuantite());
        categorieDto.setId(livre.getCategorie().getId());
        categorieDto.setNom(livre.getCategorie().getNom());
        livreDto.setCategorieDto(categorieDto);
        userDto.setEmail(livre.getUser().getEmail());
        userDto.setId(livre.getUser().getId());
        userDto.setFirstname(livre.getUser().getFirstname());
        userDto.setLastname(livre.getUser().getLastname());
        userDto.setEmail(livre.getUser().getEmail());
        livreDto.setUserDto(userDto);
        return livreDto;
    }
}
